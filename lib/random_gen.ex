defmodule RandomGen do
  @moduledoc """
  Tutto quello che serve per codificare in base n aria dei numeri partendo da simboli customizzabili

  In config basta configurare il charset e in base alla lunghezza di questo encode converte
  il numero in base n usando i simboli nel charset.

  Es. se nel charset ho ["0", "1"] encode funzionerà da convertitore binario
  """

  # @default_charset = ["A", "B"]



  @doc """
  Crea 10000 numeri partendo da from
  Li randomizza
  Li converte in base n_aria
  Fa il padding della stringa in modo che abbia 6 caratteri
  """
  def generate(from, range \\ 10000) do
    (from + 1)..(from + range)
    |> Enum.shuffle
    |> Enum.map(
      fn n -> encode(n)
      |> String.pad_leading(6, (charset() |> Enum.at(0)))
     end)
  end

  def encode(n) do
    encode(n, charset())
  end

  def encode(0, _) do
    ""
  end

  def encode(n, charset) when length(charset) > 1 do
    base = charset |> Enum.count()

    number = div(n, base)
    remainder = rem(n, base)

    encode(number, charset) <> (charset |> Enum.at(remainder))
  end


  defp charset do
    Application.fetch_env!(:random_gen, :charset)
  end

end
