defmodule RandomGen.NumberIndex do
  @moduledoc """
  Contiene la logica per leggere e scrivere la variabile che indica da che numero andare a generare
  i prossimi 10000 codici.
  Al momento semplicemente ritorna una variabile, poi sarà da collegare ad un db per la persistenza
  """


  use GenServer

  def start_link(n) do
    IO.inspect("RandomGen.NumberIndex.start_link(:ok)")
    GenServer.start_link(__MODULE__, n, name: __MODULE__)
  end

  @impl true
  def init(state) do
    IO.inspect("RandomGen.NumberIndex.init(#{state})")
    {:ok, state}
  end

  @impl true
  def handle_call({:set, n}, _from, _) do
    {:reply, n, n}
  end

  @impl true
  def handle_call(:get, _from, old_state) do
    {:reply, old_state, old_state}
  end

end
