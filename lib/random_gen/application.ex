defmodule RandomGen.Application do

  @moduledoc """
  Application lauching supervisor launching 2 GenServer
  """

  use Application

  @spec start(any, any) :: {:error, any} | {:ok, pid}
  def start(_type, _args) do
    IO.puts("RandomGen.Application.start()")
    children = [
      %{
        id: RandomGen.NumberQueue,
        start: {RandomGen.NumberQueue, :start_link, [:ok]}
      },
      %{
        id: RandomGen.NumberIndex,
        start: {RandomGen.NumberIndex, :start_link, [Application.fetch_env!(:random_gen, :first_number)]}
      }
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: RandomGen.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
