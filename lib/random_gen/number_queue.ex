defmodule RandomGen.NumberQueue do

  @moduledoc """
  Gestisce la coda, il client utilizza start link dal supervisore.
  e RandomGen.NumberQueue.pop\0 per ricevere il primo elemento della lista
  """

  use GenServer

  #  Client callbacks

  def start_link(:ok) do
    IO.inspect("RandomGen.NumberQueue.start_link(:ok)")
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def pop() do
    GenServer.call(RandomGen.NumberQueue, :pop)
  end


  # Process callbacks

  @impl true
  def init(state) do
    IO.inspect("RandomGen.NumberQueue.init(#{state})")
    {:ok, state}
  end

  @impl true
  def handle_call(:pop, _from, []) do
    first_number = GenServer.call(RandomGen.NumberIndex, :get)
    [head | tail] = RandomGen.generate(0)
    GenServer.call(RandomGen.NumberIndex, {:set, first_number + 10000})
    {:reply, head, tail}
  end

  @impl true
  def handle_call(:pop, _from, [head | tail]) do
    {:reply, head, tail}
  end

end
