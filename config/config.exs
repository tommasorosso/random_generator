import Config

config :random_gen,
  charset: ["A", "B", "C", "D", "E", "F", "G", "H", "I", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
  first_number: 0
