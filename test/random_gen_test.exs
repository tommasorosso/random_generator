defmodule RandomGenTest do
  use ExUnit.Case
  doctest RandomGen

  test "Encoder to hex" do
    assert RandomGen.encode(1128, [
             "0",
             "1",
             "2",
             "3",
             "4",
             "5",
             "6",
             "7",
             "8",
             "9",
             "A",
             "B",
             "C",
             "D",
             "E",
             "F"
           ]) == "468"
  end


  test "All numbers should be different" do
    a_lot_of_numbers = [0..1000000] |> Enum.map(fn _ -> RandomGen.NumberQueue.pop() end)

    assert (Enum.count(a_lot_of_numbers)) == (a_lot_of_numbers |> Enum.uniq |> Enum.count)
  end

end
